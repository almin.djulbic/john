#!/usr/bin/make -f

export DH_VERBOSE=1
# Temporarily workaround GCC 12 generating broken SIMD
export CC=gcc-11
export CXX=g++-11

# This is for the target selection
CPU := $(shell dpkg-architecture -qDEB_HOST_ARCH_CPU)
OS := $(shell dpkg-architecture -qDEB_HOST_ARCH_OS)

# selecting compilation target
ifeq ($(OS),hurd)
	TARGET := generic
else ifeq ($(CPU),i386)
	# targeting an unoptimized binary first
	TARGET := $(OS)-x86-any
else ifeq ($(CPU),amd64)
	# targeting an unoptimized binary first
	TARGET := $(OS)-x86-64
else ifeq ($(CPU),powerpc)
	TARGET := linux-ppc32
else ifeq ($(CPU),ppc64)
	TARGET := linux-ppc64
else ifeq ($(CPU),alpha)
	TARGET := linux-alpha
else ifeq ($(CPU),sparc)
	TARGET := linux-sparc
else ifeq ($(CPU),ia64)
	TARGET := linux-ia64
else
	TARGET := generic
endif

# This is to add missing hardening fortify functions export
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND=$(shell dpkg-buildflags --get CPPFLAGS)
export DEB_LDFLAGS_MAINT_APPEND=

# Custom compilation flags
DEB_CFLAGS_MAINT_APPEND += -DJOHN_SYSTEMWIDE -DJOHN_SYSTEMWIDE_EXEC=\"/usr/lib/john\" -DCFG_FULL_NAME=\"/etc/john/john.conf\"

ifeq ($(OS),kfreebsd)
	# enabling crypt(3) for kfreebsd, there is a patch adding fmt_c3.o
	DEB_CFLAGS_MAINT_APPEND += -DHAVE_CRYPT
	DEB_LDFLAGS_MAINT_APPEND += -lcrypt
	# as we will use $(OS)-x86-*, it must be named freebsd, not kfreebsd
	OS := freebsd
endif

# testing i386 for mmx instructions and sse2 extensions
ifeq ($(CPU),i386)
	ifneq ($(OS),hurd)
		HAVEMMX := 1
		HAVESSE := 1
	endif
endif
# testing amd64 for xop and avx instructions (linux only)
ifeq ($(OS)-$(CPU),linux-amd64)
	HAVEXOP := 1
	HAVEAVX := 1
	HAVEAVX2 := 1
	HAVEAVX512F := 1
	HAVEAVX512BW := 1
endif

define build_target_cpu =
cd src && ./configure --disable-native-tests --disable-openmp CPPFLAGS='$(patsubst %,-m%,$(filter-out base,$(1)))'
$(MAKE) -C src clean
$(MAKE) -C src -j4
cp run/john run/john-$(1)-non-omp
if test -e debian/last-build; then \
    cd src && ./configure --disable-native-tests --enable-openmp CPPFLAGS='-m$(1) -DOMP_FALLBACK -DOMP_FALLBACK_BINARY="\"john-$(1)-non-omp\"" -DCPU_FALLBACK -DCPU_FALLBACK_BINARY="\"'"$$(cat ../debian/last-build)"'\""'; \
else \
	cd src && ./configure --disable-native-tests CPPFLAGS='-DOMP_FALLBACK -DOMP_FALLBACK_BINARY="\"john-$(1)-non-omp\""'; \
fi
$(MAKE) -C src clean
$(MAKE) -C src -j4
cp run/john run/john-$(1)-omp
echo "john-$(1)-omp" > debian/last-build
endef


%:
	dh $@ --sourcedirectory=src --with python3

override_dh_auto_configure:
	# Skip it

override_dh_auto_build:
	$(call build_target_cpu,base)
ifeq ($(HAVEAVX),1)
	$(call build_target_cpu,avx)
endif
ifeq ($(HAVEXOP),1)
	$(call build_target_cpu,xop)
endif
ifeq ($(HAVEAVX2),1)
	$(call build_target_cpu,avx2)
endif
ifeq ($(HAVEAVX512F),1)
	$(call build_target_cpu,avx512f)
endif
ifeq ($(HAVEAVX512BW),1)
	$(call build_target_cpu,avx512bw)
endif

ifeq ($(HAVEMMX),1)
	$(call build_target_cpu,mmx)
endif
ifeq ($(HAVESSE),1)
	$(call build_target_cpu,sse)
endif
	ln -sf "../lib/john/$$(cat debian/last-build)" debian/last-build-link

execute_after_dh_auto_install:
	# install link john in usr/sbin
	mkdir -p debian/john/usr/sbin
	mv debian/last-build-link debian/john/usr/sbin/john
	# install fallbacks as needed
	for binary in run/john-*; do \
	    test -x $$binary && dh_install $$binary /usr/lib/john/ || true; \
	done
	# install shell completions
	mkdir -p debian/john/usr/share/bash-completion/completions/
	cp run/john.bash_completion debian/john/usr/share/bash-completion/completions/john
	mkdir -p debian/john/usr/share/zsh/vendor-completions/
	cp run/john.zsh_completion debian/john/usr/share/zsh/vendor-completions/_john

override_dh_link:
	dh_link
	for ext in pl py; do \
	    for script in run/*john.$$ext; do \
	        test -x $$script \
	          && dh_link -pjohn-data /usr/share/john/$$(basename $$script) /usr/bin/$$(basename $$script | sed "s/\.$$ext//") \
	          || true; \
	    done \
	done

override_dh_python3:
	dh_python3 --shebang="/usr/bin/env python3" /usr/share/john

execute_after_dh_auto_clean:
	# script-not-executable
	chmod +x debian/extra/cronjob	

override_dh_auto_test:

override_dh_installdocs:
	dh_installdocs -XCONTACT -XLICENSE -XCOPYING -XINSTALL -Xextras -XFreeBSD
