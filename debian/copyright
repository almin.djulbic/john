Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: John the Ripper
Upstream-Contact: Alexander Peslyak (Solar Designer) <solar@openwall.com>
Source: https://www.openwall.com/john/

Files: *
Copyright: © 1996-2019, Solar Designer <solar@openwall.com>
License: GPL-2-with-exceptions
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; version 2.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 As a special exception to the GNU General Public License terms,
 permission is hereby granted to link the code of this program, with or
 without modification, with any version of the OpenSSL library and/or any
 version of unRAR, and to distribute such linked combinations.  You must
 obey the GNU GPL in all respects for all of the code used other than
 OpenSSL and unRAR.  If you modify this program, you may extend this
 exception to your version of the program, but you are not obligated to
 do so.  (In other words, you may release your derived work under pure
 GNU GPL version 2 or later as published by the FSF.)
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: run/diskcryptor2john.py
Copyright: 2018, Ivan Freed <ivan.freed at protonmail.com>
License: other
 Redistribution and use in source and binary forms, with or without
 modification, are permitted.

Files: src/packaging/com.openwall.John.appdata.xml
Copyright: Claudio André <claudioandre.br at gmail.com>
License: GFDL-1.3
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 or any
 later version published by the Free Software Foundation; with no Invariant
 Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the
 license is included in the section entitled "GNU Free Documentation
 License".
 .
 If you have Invariant Sections, Front-Cover Texts and Back-Cover Texts,
 replace the "with...Texts." line with this:
 .
 with the Invariant Sections being LIST THEIR TITLES, with the Front-Cover
 Texts being LIST, and with the Back-Cover Texts being LIST.
 .
 If you have Invariant Sections without Cover Texts, or some other
 combination of the three, merge those two alternatives to suit the
 situation.
 .
 If your document contains nontrivial examples of program code, we
 recommend releasing these examples in parallel under your choice of free
 software license, such as the GNU General Public License, to permit their
 use in free software.
 .
 On Debian systems, the full text of the GNU Free Documentation
 License version 1.3 can be found in the file
 `/usr/share/common-licenses/GFDL-1.3'.

Files: debian/*
Copyright: 2018-2019 Sophie Brun <sophie@offensive-security.com>
           © 2009-2013, Ruben Molina <rmolina@udea.edu.co>
           © 2014, Julián Moreno Patiño <julian@debian.org>
           © 2008-2009. David Paleino <d.paleino@gmail.com>
           © 2004-2006, Guilherme de S. Pastore <gpastore@debian.org>
           © 2003-2004, Javier Fernandez-Sanguino Peña <jfs@computer.org>
           © 2000-2004, Christian Kurz <shorty@debian.org>
License: GPL-2+

Files: debian/man/*
Copyright: © 1999-2001, Jordi Mallach <jordi@debian.org>
           © 1999-2001, Jeronimo Pellegrini <pellegrini@mpcnet.com.br>
License: GPL-2+

Files:src/MD5_std.*
Copyright: © 1996-2006, Solar Designer <solar@openwall.com>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; version 2 or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: src/crc32.*
Copyright: © 1998-2005, Solar Designer <solar@openwall.com>
License: public-domain
 Written by Solar Designer <solar at openwall.com> in 1998, revised in
 2005 for use in John the Ripper, and placed in the public domain.

Files: src/BF_std.*
Copyright: © 1996-2010, Solar Designer <solar@openwall.com>
License: public-domain
 A public domain version of this code, with reentrant and crypt(3)
 interfaces added, but optimizations specific to password cracking
 removed, is available at: http://www.openwall.com/crypt/

Files: src/nonstd.c
       src/sboxes.c
Copyright: © 1998, Matthew Kwan <mkwan@darkside.com.au>
License: permissive
 This software may be modified, redistributed, and used for any purpose,
 so long as its origin is acknowledged.
